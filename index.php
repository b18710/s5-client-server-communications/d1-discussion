<?php

$tasks = ['Getgit', "Bake HTML", "Eat CSS", "Learn PHP"];

if(isset($_GET['index'])){ //isset() checks for the presence of the given global variable (in this case $_GET['index']. If the specific global variable exists, it will rende true. If not, it will render false)
    $indexGet = $_GET['index']; //the value of $_GET is the same asthe value in the key/value pair in the url params.

    echo "The retrieved task from GET is $tasks[$indexGet]"; 
}


if(isset($_POST['index'])){ //$_POST works the same as $GET, except the data being passed is not visible
    $indexGet = $_POST['index']; 

    echo "The retrieved task from POST is $tasks[$indexGet]"; 
}


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP SC S5</title>
</head>
<body>
        <h1>Task index from GET</h1>

        <form method="GET" action="">

          

            <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

            <button type="submit">GET</button>

        </form>

        <h1>Task index from POST</h1>

        <form method="POST">

        <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
        </select>

            <button type="submit">POST</button>

        </form>
</body>
</html>